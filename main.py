from machine import Pin
from time import sleep
from servo import move_forward, move_backward, stop
from encoder import getDistance, resetEncoder
from buttons import up_button, down_button, mode_button
from endstop import endstop
from WIFI import serve, open_socket, init_wifi

max_encoder_value = 100
mode_button_lock = False

ip = init_wifi()
connection=open_socket(ip)

manual_mode = True
# pseduo enum for manual mode 
# STOP, MOVE_UP, MOVE_DOWN
manual_mode_state = 'STOP'
# pseduo enum for auto mode
# STOP, OPEN, CLOSE
auto_mode_state = 'STOP'

def endstop_reached(pin):
    global manual_mode
    manual_mode = False
    global auto_mode_state
    auto_mode_state = 'STOP'
    resetEncoder()
    
    
def mode_button_pressed(pin):
    global mode_button_lock
    if not mode_button_lock and mode_button.value():
        global manual_mode
        manual_mode = not manual_mode
        mode_button_lock = True
    else:
        mode_button_lock = False

endstop.irq(trigger=Pin.IRQ_FALLING, handler=endstop_reached)
mode_button.irq(trigger=Pin.IRQ_FALLING, handler=mode_button_pressed)


def open_blind():
    print('open_blind')
    print(getDistance())
    while getDistance() >= -5 and getDistance() < max_encoder_value:
        move_forward()
    
    stop()    
    global auto_mode_state
    auto_mode_state = 'STOP'
        
def close_blind():
    print('close_blind')
    while endstop.value():
        move_backward()
        
    stop()
    resetEncoder()
    global auto_mode_state
    auto_mode_state = 'STOP'


def handelButtons():
    global manual_mode_state
    if up_button.value():
        move_forward()
    elif down_button.value():
       move_backward()
    else:
        stop()

try:
    if ip is not None:
        while True:
            serve(connection, open_blind, close_blind, stop, getDistance())
except KeyboardInterrupt:
    print('KeyboardInterrupt')
    connection.close()
    
    
    # if manual_mode: # Manual mode 
    #     handelButtons()
    # else: # Auto mode
    #     if auto_mode_state == 'OPEN':
    #         open_blind()
    #     elif auto_mode_state == 'CLOSE':
    #         close_blind()
    #     else:
    #         stop()
