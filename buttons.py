from machine import Pin

up_button = Pin(4, Pin.IN, Pin.PULL_DOWN)
down_button = Pin(5 , Pin.IN, Pin.PULL_DOWN)
mode_button = Pin(6, Pin.IN, Pin.PULL_DOWN)