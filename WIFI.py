import network
import time
import socket


#conecting to wifi

def init_wifi():
    SSID = "Moria"
    PASSWORD = "Barahir6"
    
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(SSID, PASSWORD)

    # Wait for connect or fail
    wait = 20
    while wait > 0:
        if wlan.status() < 0 or wlan.status() >= 3:
            break
        wait -= 1
        print('waiting for connection...')
        time.sleep(1)
    
    # Handle connection error
    if wlan.status() != 3:
        print('wifi connection failed')
        return False
    else:
        print('connected')
        ip=wlan.ifconfig()[0]
        print('IP: ', ip)
        return ip
    
def webpage(value):
    html = f"""
            <!DOCTYPE html>
             <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="css/pico.min.css">
                <title>Roleta v2</title>
            </head>
            <html>
            <body>
            <form action="./open" style="width: 100vw;">
            <input type="submit" value="open" style="width: 50vw; margin-left: 23vw; margin-right: 25vw;height: 80px;margin-top: 20px;font-size: 25px" />
            </form>
            <form action="./close" style="width: 100vw;">
            <input type="submit" value="close" style="width: 50vw; margin-left: 23vw; margin-right: 25vw;height: 80px;margin-top: 20px;font-size: 25px" />
            </form>
            <form action="./stop" style="width: 100vw;">
            <input type="submit" value="stop" style="width: 50vw; margin-left: 23vw; margin-right: 25vw;height: 80px;margin-top: 20px;font-size: 25px" />
            </form>
            <div style="margin-left: 23vw;margin-top: 20px;font-size: 15px" >Distance is {value} mm</div>
            </body>
            </html>
            """
    return html

def serve(connection, open, close, stop, distance=0):
    client = connection.accept()[0]
    request = client.recv(1024)
    request = str(request)
    try:
        request = request.split()[1]
    except IndexError:
        pass
    
    if request == '/open?':
        if open != None:
            open()
    elif request == '/close?':
        if close != None:
            close()
    elif request == '/stop?':
        if stop != None:
            stop()

    value='%.2f'%distance    
    html=webpage(value)
    client.send(html)
    client.close()     
def open_socket(ip):
    # Open a socket
    address = (ip, 81)
    connection = socket.socket()
    connection.bind(address)
    connection.listen(1)
    print(connection)
    print('listening on', address)
    return(connection)

